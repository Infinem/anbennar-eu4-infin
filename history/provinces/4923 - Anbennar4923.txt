owner = Y16
controller = Y16
add_core = Y16
culture = northern_yan
religion = righteous_path

hre = no

base_tax = 2
base_production = 4
base_manpower = 2

trade_goods = gems

capital = ""

is_city = yes
